export default function before (n, func) {
  // TODO:
  //   Creates a function that invokes func while it's called less than `n` times.
  //   Please read the test to get how it works.
  // <-start-
  // throw n is not a number
  if (isNaN(n)) {
    return () => {};
  }

  let count = 0;
  return () => {
    count++;
    if (count < n) {
      return func();
    }
  };
  // --end-->
}
